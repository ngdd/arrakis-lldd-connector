# arrakis-lldd-connector

**arrakis-lldd-connector** is a service which consumes the lldd Kafka stream and publishes (connects) to Arrakis

## Installation

```
git clone git@git.ligo.org:ngdd/arrakis-lldd-connector.git
cd arrakis-lldd-connector
pip install .
```

## Usage

This service exposes a single program, `arrakis-lldd-connector`, to push data
from the low latency data distribution (lldd) into Arrakis, serialized as Arrow
Flight record batches.
