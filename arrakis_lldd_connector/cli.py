# Copyright (c) 2022, California Institute of Technology and contributors
#
# You should have received a copy of the licensing terms for this
# software included in the file "LICENSE" located in the top-level
# directory of this package. If you did not, you can view a copy at
# https://git.ligo.org/ngdd/daqd-kafka-connector/-/raw/main/LICENSE

import logging
import sys

logger = logging.getLogger("arrakis-lldd-connector")


def add_logging_opts(parser):
    """Add logging client options to an argument parser.

    Parameters
    ----------
    parser
        An ArgumentParser instance to add client options to.

    """
    group = parser.add_mutually_exclusive_group()
    group.add_argument(
        "-q",
        "--quiet",
        action="store_true",
        help="If set, only display warnings and errors.",
    )
    group.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        help="If set, display additional logging messages.",
    )


def get_log_level(args):
    """Determine the log level from logging options.

    Parameters
    ----------
    args
        The parsed argparse arguments.

    Returns
    -------
    int
        The logging log level.

    """
    if args.quiet:
        return logging.WARNING
    elif args.verbose:
        return logging.DEBUG
    else:
        return logging.INFO


def set_up_logger(args, name="arrakis-lldd-connector"):
    """Set up common logging settings for CLI usage.

    Parameters
    ----------
    args
        The parsed argparse arguments.

    """
    logger = logging.getLogger()
    log_level = get_log_level(args)
    logger.setLevel(log_level)
    handler = logging.StreamHandler(sys.stderr)
    handler.setLevel(log_level)
    formatter = logging.Formatter(f"%(asctime)s | {name} : %(levelname)s : %(message)s")
    handler.setFormatter(formatter)
    logger.addHandler(handler)
