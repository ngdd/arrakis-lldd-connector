# Copyright (c) 2022, California Institute of Technology and contributors
#
# You should have received a copy of the licensing terms for this
# software included in the file "LICENSE" located in the top-level
# directory of this package. If you did not, you can view a copy at
# https://git.ligo.org/ngdd/daqd-kafka-connector/-/raw/main/LICENSE

"""Publish lldd frames into Arrakis.

"""

import argparse
import logging
import os
import tempfile

import arrakis
from arrakis import SeriesBlock, Time
from gwpy.io.gwf import get_channel_names
from gwpy.timeseries import TimeSeriesDict
from igwn_lldd_common.cli import (str2bool, add_status_update_options,
                                  add_topic_partition_options,
                                  extract_topic_partition_info)
from igwn_lldd_common.framekafkaconsumer import FrameKafkaConsumer

from . import cli

logger = logging.getLogger("daqd-kafka-connector")


def set_up_cli():
    """Set up the CLI parser.

    Returns
    -------
    argparse.ArgumentParser
        The argument parser.

    """
    parser = argparse.ArgumentParser()
    cli.add_logging_opts(parser)
    add_topic_partition_options(parser)

    # needed for igwn-lldd-common arg parsing for some reason
    add_status_update_options(parser)
    parser.add_argument(
        "-lkp",
        "--load-kafka-python",
        action="store_const",
        const=True,
        default=False,
        help="load kafka-python rather than confluent-kafka",
    )
    parser.add_argument(
        "-x",
        "--exit-if-missing-topics",
        action="store_const",
        const=True,
        default=False,
        help="exit if any topics are missing",
    )
    parser.add_argument(
        "-pt",
        "--poll-timeout",
        type=int,
        default=1000,
        help="Timeout when doing consumer.poll() [in ms]. Default: 1000.",
    )
    parser.add_argument(
        "-pr",
        "--poll-max-records",
        type=int,
        default=1,
        help="Max records returned when doing consumer.poll(). Default: 1.",
    )
    parser.add_argument(
        "-ff",
        "--fast-forward",
        type=str2bool,
        default=True,
        help="fast forward if fall behind",
    )
    parser.add_argument(
        "-s",
        "--ssl",
        action="store_const",
        const=True,
        default=False,
        help="use ssl"
    )
    parser.add_argument("-g", "--group-id", type=str, help="Kafka group ID")


    parser.add_argument(
        "-u",
        "--arrakis-url",
        required=True,
        help="set Arrakis URL to connect to",
    )
    parser.add_argument(
        "-b",
        "--bootstrap-servers",
        required=True,
        help="set lldd Kafka broker(s) to connect to",
    )

    return parser


def main():
    """Publish daqd buffers into Kafka."""
    # set up / parse arguments
    parser = set_up_cli()
    args = parser.parse_args()

    # set up logging
    cli.set_up_logger(args)

    # Get the topics from the topic partition
    tp_info = extract_topic_partition_info(args, key_by_topic=True)

    # create arrakis client
    client = arrakis.connect(args.arrakis_url)
    logger.info(f"connected to {args.arrakis_url}")

    # register client for producing
    assert len(args.ifo) == 1, "only one detector allowed currently"
    client.register("lldd", args.ifo[0])

    # set up lldd frame consumer
    frameconsumer = FrameKafkaConsumer(args, tp_info)

    # start publishing data
    try:
        while True:
            for frame_buffer, payload_info in frameconsumer.poll_and_extract(tp_info):
                # continue if the full frame has not been assembled
                if not frame_buffer:
                    continue

                t0 = payload_info["data_gps_timestamp"]
                topic = payload_info["topic"]
                duration = tp_info[topic]["delta_t"]

                # publish
                with tempfile.NamedTemporaryFile(delete=False, suffix="gwf") as fp:
                    # NOTE: the frame reading library requires files to be on disk
                    fp.write(frame_buffer)
                    fp.close()

                    series = {}
                    channels = get_channel_names(fp.name)
                    for channel, data in TimeSeriesDict.read(fp.name, channels=channels).items():
                        series[channel] = data.value
                    logger.info("publishing data with timestamp t=%d", t0)
                    block = SeriesBlock(t0 * Time.SECONDS, series, duration=duration)

                    client.publish(block)
                    os.unlink(fp.name)

    except KeyboardInterrupt:
        pass
    finally:
        # wait until all messages are sent
        logger.info("shutting down client")
        client.close()


if __name__ == "__main__":
    main()
